# Function receives list A of characters. Not a string to allow O(1) memory solution.
# Substitutes all whitepace character sequences with single space symbol.
# Modifies list inplace, shifting all valueable characters to the beginning of a string.
# Returns same list A and amount N of meaningful characters in it. Characters in A[N:] are undefined.
def compress_spaces(symbol_list):
    is_prev_space = False
    compressed_list_index = 0
    for symbol in symbol_list:
        if not is_prev_space or not symbol.isspace():
            symbol_list[compressed_list_index] = symbol
            compressed_list_index += 1
        is_prev_space = symbol.isspace()
    return symbol_list, compressed_list_index


if __name__ == "__main__":
    print(f"{compress_spaces([]) = }")
    print(f"{compress_spaces(['a', ' ', 'b']) = }")
    print(f"{compress_spaces(['a', ' ', ' ', ' ', 'b']) = }")
    print(f"{compress_spaces([' ', ' ', 'a', ' ', ' ', ' ', 'b', ' ', ' ']) = }")