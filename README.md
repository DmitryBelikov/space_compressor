# Space compressor

Simple function, which compressed subsequent space characters to one using O(1) additional memory and O(n) time.
